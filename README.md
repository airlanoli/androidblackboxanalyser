### BETA version
AndroidBlackBoxAnalyser is under development and the first non-beta version is not released yet.

# AndroidBlackBoxAnalyser
AndroidBlackBoxAnalyser is an android port of [Plasmatree PID-Analyzer](https://github.com/Plasmatree/PID-Analyzer).
It use [blackbox-tools](https://github.com/cleanflight/blackbox-tools) to decode blackbox and call PID-analyzer to render graph.
The android code add some features to improve user experience and run, manage, navigate easily your analysis.

## Screenshots
![home][home]
![menu][menu]
![progress][progress]
![header][header]
![noise][noise]
![response][response]

## Install application
### From the playstore :
install the app directly from the [playstore](https://play.google.com/store/apps/details?id=com.androidblackboxanalyser)
### Manually with the APK :
Download APK on the [last release](https://gitlab.com/bibitte/androidblackboxanalyser/-/releases) and install it on your android device.
(You need to authorise unknown source)

## install project (for developer)
1. Clone the project :
```shell
git clone https://gitlab.com/bibitte/androidblackboxanalyser.git
```
2. Update submodules :
```shell
cd androidblackboxanalyser/
git submodule init
git submodule update
```
3. create file local.properties on the root directory
4. import project on android studio or your favorite IDE (build project with gradle)     

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to create or update tests as appropriate.

## License
[GNU GENERAL PUBLIC LICENSE Version 3](https://www.gnu.org/licenses/gpl-3.0.html)

[home]: doc/images/home.png "home"
[menu]: doc/images/menu.png "menu"
[progress]: doc/images/progress.png "progress"
[header]: doc/images/header.png "header"
[noise]: doc/images/noise.png "noise"
[response]: doc/images/response.png "response"