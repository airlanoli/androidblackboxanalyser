package com.androidblackboxanalyser;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.androidblackboxanalyser.utils.JSonUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.TreeMap;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class JSonUtilsTest {

    private static final String JSON_STR ="[\n" +
            "  {\n" +
            "    \"number\": 0,\n" +
            "    \"headerMap\": {\n" +
            "      \"header1\": \"value1\",\n" +
            "      \"header2\": \"value2\",\n" +
            "      \"header3\": \"value3\"\n" +
            "    }\n" +
            "  },\n" +
            "  {\n" +
            "    \"number\": 1,\n" +
            "    \"headerMap\": {\n" +
            "      \"header4\": \"value4\",\n" +
            "      \"header5\": \"value5\",\n" +
            "      \"header6\": \"value6\"\n" +
            "    }\n" +
            "  },\n" +
            "  {\n" +
            "    \"number\": 3,\n" +
            "    \"headerMap\": {\n" +
            "      \"headertest7\": \"valuetest7\",\n" +
            "      \"headertest8\": \"valuetest8\",\n" +
            "      \"headertest9\": \"valuetest9\"\n" +
            "    }\n" +
            "  }\n" +
            "]";
    public static final String NAME = "nameTest";


    private ArrayList<Record> createListRecord(){
        ArrayList<Record> list = new ArrayList<>();
        int number = 0;
        TreeMap<String,String> headerMap = new TreeMap<>();
        headerMap.put("header1","value1");
        headerMap.put("header2","value2");
        headerMap.put("header3","value3");

        list.add(new Record(NAME,number,headerMap));

        number = 1;
        headerMap = new TreeMap<>();
        headerMap.put("header4","value4");
        headerMap.put("header5","value5");
        headerMap.put("header6","value6");

        list.add(new Record(NAME,number,headerMap));

        number = 3;
        headerMap = new TreeMap<>();
        headerMap.put("headertest7","valuetest7");
        headerMap.put("headertest8","valuetest8");
        headerMap.put("headertest9","valuetest9");

        list.add(new Record(NAME,number,headerMap));
        return list;
    }

    @Test
    public void writeJsonStream() {

        ArrayList<Record> list = createListRecord();

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        try {
            JSonUtils.writeJsonStream(byteArrayOutputStream,list);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String result = null;
        try {
            result =  byteArrayOutputStream.toString(StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        assertEquals(JSON_STR,result);
    }

    @Test
    public void readJsonStream() {
        InputStream in = new ByteArrayInputStream(JSON_STR.getBytes());
        ArrayList<Record> list = null;
        try {
            list = JSonUtils.readJsonStream(NAME,in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(createListRecord(),list);
    }


}