#ifndef BATTERY_H_BLACKBOX_DECODE_H_
#define BATTERY_H_BLACKBOX_DECODE_H_
/*
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <math.h>

#include <errno.h>
#include <fcntl.h>

//For msvcrt to define M_PI:
#include <math.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
*/

extern "C"
{
#include "blackbox-tools/src/parser.h"
#include "blackbox-tools/src/platform.h"
#include "blackbox-tools/src/tools.h"
#include "blackbox-tools/src/gpxwriter.h"
#include "blackbox-tools/src/imu.h"
#include "blackbox-tools/src/battery.h"
#include "blackbox-tools/src/units.h"
#include "blackbox-tools/src/stats.h"
}


typedef struct decodeOptions_t {
    int help, raw, limits, debug, toStdout;
    int logNumber;
    int simulateIMU, imuIgnoreMag;
    int simulateCurrentMeter;
    int mergeGPS;
    const char *outputPrefix;

    bool overrideSimCurrentMeterOffset, overrideSimCurrentMeterScale;
    int16_t simCurrentMeterOffset, simCurrentMeterScale;

    Unit unitGPSSpeed, unitFrameTime, unitVbat, unitAmperage, unitHeight, unitAcceleration, unitRotation, unitFlags;
} decodeOptions_t;


//We'll use field names to identify GPS field units so the values can be formatted for display
typedef enum {
    GPS_FIELD_TYPE_INTEGER,
    GPS_FIELD_TYPE_DEGREES_TIMES_10, // for headings
    GPS_FIELD_TYPE_COORDINATE_DEGREES_TIMES_10000000,
    GPS_FIELD_TYPE_METERS_PER_SECOND_TIMES_100,
    GPS_FIELD_TYPE_METERS
} GPSFieldType;


static void fprintfMilliampsInUnit(FILE *file, int32_t milliamps, Unit unit);

static void fprintfMicrosecondsInUnit(FILE *file, int64_t microseconds, Unit unit);

static bool fprintfMainFieldInUnit(flightLog_t *log, FILE *file, int fieldIndex, int64_t fieldValue, Unit unit);

void onEvent(flightLog_t *log, flightLogEvent_t *event);

void outputFieldNamesHeader(FILE *file, flightLogFrameDef_t *frame, Unit *fieldUnit, bool skipTime);

void createGPSCSVFile(flightLog_t *log);

static void updateSimulations(flightLog_t *log, int64_t *frame, int64_t currentTime);

void outputGPSFields(flightLog_t *log, FILE *file, int64_t *frame);

void outputGPSFrame(flightLog_t *log, int64_t *frame);

void outputSlowFrameFields(flightLog_t *log, int64_t *frame);

void outputMainFrameFields(flightLog_t *log, int64_t frameTime, int64_t *frame);

void outputMergeFrame(flightLog_t *log);

void updateFrameStatistics(flightLog_t *log, int64_t *frame);

void onFrameReadyMerge(flightLog_t *log, bool frameValid, int64_t *frame, uint8_t frameType, int fieldCount, int frameOffset, int frameSize);

void onFrameReady(flightLog_t *log, bool frameValid, int64_t *frame, uint8_t frameType, int fieldCount, int frameOffset, int frameSize);

void resetGPSFieldIdents();

void identifyGPSFields(flightLog_t *log);

void applyFieldUnits(flightLog_t *log);

void writeMainCSVHeader(flightLog_t *log);

void onMetadataReady(flightLog_t *log);

void printStats(flightLog_t *log, int logIndex, bool raw, bool limits);

void resetParseState() ;

int decodeFlightLog(flightLog_t *log, const char *filename, int logIndex);

int validateLogIndex(flightLog_t *log);

void printUsage(const char *argv0);

double parseDegreesMinutes(const char *s);

void parseCommandlineOptions(int argc, char **argv);

int main(int argc, char **argv);

#endif
