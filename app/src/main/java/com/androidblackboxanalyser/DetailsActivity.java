package com.androidblackboxanalyser;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.appcompat.widget.AppCompatSpinner;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.ArrayList;
import java.util.Objects;

public class DetailsActivity extends AbstractActivityWithMenu {

    public static final String RECORD_LIST_KEY = "RECORD_LSIT__KEY";
    public static final String RECORD_KEY = "RECORD_KEY";
    public static final int TAB_INDEX_HEADER = 0;
    public static final int TAB_INDEX_NOISE = 1;
    public static final int TAB_INDEX_RESPONSE = 2;
    private ArrayList<Record> recordList;
    private Record record;

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_details;
    }

    @Override
    protected void configureMainView() {

        recordList = getIntent().getParcelableArrayListExtra(RECORD_LIST_KEY);
        record = getIntent().getParcelableExtra(RECORD_KEY);

        Objects.requireNonNull(this.getSupportActionBar(),"supportActionBar can not be null").setTitle("");

        AppCompatSpinner spinner = this.findViewById(R.id.spinner);
        ArrayAdapter<Record> adapter = new ArrayAdapter<>(this, R.layout.spinner_item, recordList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setSelection(adapter.getPosition(record));

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int index, long id) {
                Record nextRecord = adapter.getItem(index);
                if (nextRecord != null && !nextRecord.equals(record)) {
                    Intent intent = new Intent(DetailsActivity.this, DetailsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putParcelableArrayListExtra(DetailsActivity.RECORD_LIST_KEY, recordList);
                    intent.putExtra(DetailsActivity.RECORD_KEY, nextRecord);
                    startActivity(intent);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ViewPager2 pager = findViewById(R.id.activity_details_viewpager);
        PageDetailsAdapter pageDetailsAdapter = new PageDetailsAdapter(this, record);
        pager.setAdapter(pageDetailsAdapter);

        TabLayout tabs = findViewById(R.id.activity_main_tabs);
        new TabLayoutMediator(tabs, pager,
                (tab, position) -> {
                    String title = "tab " + position;
                    switch (position) {
                        case TAB_INDEX_HEADER:
                            title = getString(R.string.tab_title_header);
                            break;
                        case TAB_INDEX_NOISE:
                            title = getString(R.string.tab_title_noise);
                            break;
                        case TAB_INDEX_RESPONSE:
                            title = getString(R.string.tab_title_response);
                            break;
                    }
                    tab.setText(title);
                }

        ).attach();
        // tabs have the same width
        tabs.setTabMode(TabLayout.MODE_FIXED);
    }


}
