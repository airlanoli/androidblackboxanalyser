package com.androidblackboxanalyser.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.os.Process;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import com.androidblackboxanalyser.AnalysisProgressActivity;
import com.androidblackboxanalyser.R;
import com.androidblackboxanalyser.Record;
import com.androidblackboxanalyser.utils.BlackBoxUtils;
import com.androidblackboxanalyser.utils.JSonUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AnalysisService extends Service {

    private static final String EXTRA_BLACKBOX_LOG_URI = "com.androidblackboxanalyser.service.extra.BLACKBOX_LOG_URI_KEY";
    private static final String EXTRA_ANALYSIS_NAME = "com.androidblackboxanalyser.service.extra.ANALYSIS_NAME";

    public static final int NB_STEP_OUTSIDE_ANALYSIS = 3;
    public static final int NB_STEP_INSIDE_ANALYSIS = 2;

    public static final String ACTION_START_DETAIL_AND_STOP_PROGRESS = "ACTION_START_DETAIL_AND_STOP_PROGRESS";
    public static final String EXTRA_RECORD_LIST = "EXTRA_RECORD_LIST";

    public static final String ACTION_PROGRESS = "ACTION_PROGRESS";
    public static final String EXTRA_PROGRESS_TEXT = "EXTRA_PROGRESS_TEXT";
    public static final String EXTRA_PROGRESS = "EXTRA_PROGRESS";
    public static final String EXTRA_PROGRESS_MAX = "EXTRA_PROGRESS_MAX";

    public static final String ACTION_ERROR = "ACTION_ERROR";
    public static final String EXTRA_ERROR_MSG = "EXTRA_ERROR_MSG";

    public static final String PARAM_INTENT = "INTENT";

    private static final String CHANNEL_ID = "com.androidblackboxanalyser.service.CHANNEL_ID";
    private static final int NOTIFICATION_ID = 1;

    private static final String LOG_TAG = AnalysisService.class.getName();
    public static final int INIT_PROGRESS_MAX = 20;
    public static final int INIT_PROGRESS = 1;

    private String notificationTitle ;

    private ServiceHandler serviceHandler;
    private NotificationManager notificationManager;

    private PendingIntent data;


    public AnalysisService() {
    }

    public static void startActionAnalysis(AppCompatActivity activity, Uri fileUri, String name) {

        PendingIntent pendingResult = activity.createPendingResult(
                100, new Intent(), 0);
        Intent intent = new Intent(activity, AnalysisService.class);
        intent.putExtra("pendingIntent", pendingResult);

        intent.putExtra(EXTRA_BLACKBOX_LOG_URI, fileUri);
        intent.putExtra(EXTRA_ANALYSIS_NAME, name);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            activity.startForegroundService(intent);
        } else {
            activity.startService(intent);
        }
    }


    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            // Normally we would do some work here, like download a file.
            // For our sample, we just sleep for 5 seconds.
            Intent intent = msg.getData().getParcelable(PARAM_INTENT);
            if (intent != null) {
                data = intent.getParcelableExtra("pendingIntent");
                Uri fileUri = intent.getParcelableExtra(EXTRA_BLACKBOX_LOG_URI);
                String name = intent.getStringExtra(EXTRA_ANALYSIS_NAME);
                handleActionAnalysis(fileUri, name);
            } else {
                Log.e(LOG_TAG, "Erreur intent is null in handleMaessage");
                sendErrorIntent(getApplicationContext().getString(R.string.analysis_service_error_intent_null));
            }
            // Stop the service using the startId, so that we don't stop
            // the service in the middle of handling another job
            stopSelf(msg.arg1);
        }
    }



    @Override
    public void onCreate() {
        this.notificationTitle = getString(R.string.notification_title);


        // Start up the thread running the service. Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block. We also make it
        // background priority so CPU-intensive work doesn't disrupt our UI.
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_FOREGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        Looper serviceLooper = thread.getLooper();
        serviceHandler = new ServiceHandler(serviceLooper);
    }


    /** Updates the notification. */
    private void updateNotification( String contentText, int progressMax,int progress) {
        notificationManager.notify(NOTIFICATION_ID, buildNotification(contentText,progressMax,progress));
    }

    private Notification buildNotification (String contentText, int progressMax,int progress){

        Intent notificationIntent = new Intent(getApplicationContext(), AnalysisProgressActivity.class);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 0,
                notificationIntent, 0);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            return new Notification.Builder(this, CHANNEL_ID)
                    .setContentTitle(notificationTitle)
                    .setContentText(contentText)
                    .setSmallIcon(R.drawable.ic_launcher_rounded)
                    .setProgress(progressMax,progress,false)
                    .setOnlyAlertOnce(true)
                    .setContentIntent(intent)
                    .build();
        } else {
            return new NotificationCompat.Builder(this,CHANNEL_ID)
                    .setContentTitle(notificationTitle)
                    .setContentText(contentText)
                    .setSmallIcon(R.drawable.ic_launcher_rounded)
                    .setProgress(progressMax,progress,false)
                    .setOnlyAlertOnce(true)
                    .setContentIntent(intent)
                    .build();
        }
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = serviceHandler.obtainMessage();
        msg.arg1 = startId;
        Bundle bundle = new Bundle();
        bundle.putParcelable(PARAM_INTENT, intent);
        msg.setData(bundle);
        serviceHandler.sendMessage(msg);


        notificationManager = getSystemService(NotificationManager.class);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {


            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this

            notificationManager.createNotificationChannel(channel);

        }

        Notification notification = buildNotification(getString(R.string.notification_init_message), INIT_PROGRESS_MAX, INIT_PROGRESS);
// Notification ID cannot be 0.
        startForeground(NOTIFICATION_ID, notification);
        // If we get killed, after returning from here, restart only if there are waiting intent
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }


    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionAnalysis(Uri fileUri, String name) {
        try {
            ArrayList<Record> recordList = new ArrayList<>();

            int progress = INIT_PROGRESS;
            int progressMax = INIT_PROGRESS_MAX;

            updateProgress(getString(R.string.copy_log_to_temp_dir, fileUri.getLastPathSegment()),progressMax,progress);

            File tmpLog = BlackBoxUtils.copyOnTempDir(fileUri, getApplicationContext());

            updateProgress(getString(R.string.splitting_log),progressMax,progress++);

            List<File> fileList = BlackBoxUtils.splitBBLog(tmpLog, getApplicationContext());
            int cpt = 0;

            progressMax= NB_STEP_OUTSIDE_ANALYSIS + fileList.size() * NB_STEP_INSIDE_ANALYSIS;
            for (File f : fileList) {

                updateProgress(getString(R.string.analysing_bb_decode, cpt + 1, fileList.size()),progressMax,progress++);
                BlackBoxUtils.parseLogFromJNI(f.getAbsolutePath());

                updateProgress(getString(R.string.analysing_plasmatree, cpt + 1, fileList.size()),progressMax,progress++);
                BlackBoxUtils.callPlasmatree(f, getApplicationContext(), cpt);

                Record record = new Record(name, cpt);
                record.setHeaderMap(BlackBoxUtils.readHeaderFromFile(f));
                recordList.add(record);

                cpt++;
            }

            updateProgress(getString(R.string.moving_to_dest),progressMax,progress++);

            BlackBoxUtils.moveTmpDirToRecord(name, getApplicationContext());
            updateProgress(getString(R.string.write_json),progressMax,progressMax);
            if (!recordList.isEmpty()) {
                JSonUtils.writeJson(name, recordList, getApplicationContext());
                BlackBoxUtils.cleanRecordDir(name, getApplicationContext());

            }
            startDetailActivityAndFinishProgressActivity(recordList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startDetailActivityAndFinishProgressActivity(ArrayList<Record> recordList) {
        sendParcelableListIntent(ACTION_START_DETAIL_AND_STOP_PROGRESS, EXTRA_RECORD_LIST, recordList);
    }

    private void updateProgress(String text, int progressMax, int progress) {
        Intent intent = new Intent();
        intent.setAction(ACTION_PROGRESS);
        intent.putExtra(EXTRA_PROGRESS_TEXT, text);
        intent.putExtra(EXTRA_PROGRESS_MAX, progressMax);
        intent.putExtra(EXTRA_PROGRESS, progress);
        sendIntent(intent);
        updateNotification(text,progressMax,progress);
    }


    private void sendStringIntent(String action, String key, String value) {
        Intent intent = new Intent();
        intent.setAction(action);
        intent.putExtra(key, value);
        sendIntent(intent);
    }

    private void sendParcelableListIntent(String action, String key, ArrayList<? extends Parcelable> list) {
        Intent intent = new Intent();
        intent.setAction(action);
        intent.putParcelableArrayListExtra(key, list);
        sendIntent(intent);
    }

    private void sendErrorIntent(String string) {
        sendStringIntent(ACTION_ERROR,string,EXTRA_ERROR_MSG);
    }

    private void sendIntent(Intent intent) {
        try {
            data.send(this,200,intent);
        } catch (PendingIntent.CanceledException e) {
            Log.e(LOG_TAG,"exception when sending Intent" ,e);
        }
    }

}
