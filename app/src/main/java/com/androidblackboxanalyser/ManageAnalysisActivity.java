package com.androidblackboxanalyser;

import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import com.androidblackboxanalyser.dialog.AlertDialogFragment;
import com.androidblackboxanalyser.dialog.LogNameDialogFragment;
import com.androidblackboxanalyser.utils.BlackBoxUtils;

import java.io.IOException;
import java.util.Objects;

public class ManageAnalysisActivity extends AbstractActivityWithMenu {
    String[] analysisArray = null;
    CheckBox[] checkBoxArray = null;


    @Override
    protected int getActivityLayout() {
        return R.layout.activity_manage_analysis;
    }

    @Override
    protected void configureMainView() {
        this.analysisArray = BlackBoxUtils.listRecord(this);
        if(this.analysisArray!=null){

            TableLayout tl = this.findViewById(R.id.manage_analysis_tab);
            Button deleteButton = this.findViewById(R.id.manage_delete_button);
            deleteButton.setOnClickListener(view -> deleteSelected()

            );


            TypedValue out = new TypedValue();
            getResources().getValue(R.dimen.manage_tab_analysis_layout_weight, out, true);
            float analysisLayoutWeight = out.getFloat();
            out = new TypedValue();
            getResources().getValue(R.dimen.manage_tab_rename_layout_weight, out, true);
            float renameLayoutWeight = out.getFloat();
            out = new TypedValue();
            getResources().getValue(R.dimen.manage_tab_delete_layout_weight, out, true);
            float deleteLayoutWeight = out.getFloat();

            this.checkBoxArray = new CheckBox[this.analysisArray.length];
            int i = 0;

            for(String str : this.analysisArray){
                // Create a new row to be added.
                TableRow tr = new TableRow(this);
                tr.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT));

                // Create a textView to be the row-content.
                TextView textView = new TextView(this);
                textView.setText(str);
                textView.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT,analysisLayoutWeight));
                textView.setGravity(Gravity.CENTER);
                tr.addView(textView);

                ImageButton button = new ImageButton(this);
                button.setImageResource(R.drawable.ic_edit_black_24dp);
                button.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT,renameLayoutWeight));
                int finalI = i;
                button.setOnClickListener(arg0 -> rename(finalI));
                tr.addView(button);

                CheckBox checkBox = new CheckBox(this);
                checkBox.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT,deleteLayoutWeight));
                checkBox.setGravity(Gravity.CENTER);
                tr.addView(checkBox);
                checkBoxArray[i]=checkBox;

                // Add row to TableLayout.
                tl.addView(tr);
                i++;
            }
        }
    }

    private void deleteSelected() {
        int i=0;
        for (CheckBox checkBox : this.checkBoxArray){
            if(checkBox.isChecked()){
                try {
                    BlackBoxUtils.deleteLogs(this.analysisArray[i],this);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            i++;
        }
        recreate();
    }

    private void rename(int index) {

        String oldName=analysisArray[index];
        LogNameDialogFragment dialog = new LogNameDialogFragment(getString(R.string.dialog_rename_label),oldName);
        dialog.setListener(new LogNameDialogFragment.LogNameDialogListener() {
            @Override
            public void onDialogPositiveClick(LogNameDialogFragment dialog) {
                String newName = dialog.getLogNameInput();
                if(newName!=null){
                    if(newName.equals(oldName)){
                        dialog.dismiss();
                    }else{
                        if(!BlackBoxUtils.isRecordAlreadyExist(newName,ManageAnalysisActivity.this)){

                            dialog.dismiss();
                            try {
                                BlackBoxUtils.moveRecord(oldName,newName,ManageAnalysisActivity.this);
                            } catch (IOException e) {
                                DialogFragment alertDialog = new AlertDialogFragment(getString(R.string.alert_error,e.getLocalizedMessage()));
                                alertDialog.show(getSupportFragmentManager(), "AlertDiologFragmentRenameError");
                            }
                            recreate();
                        }else{
                            dialog.setError(getString(R.string.error_record_exist,newName));
                        }
                    }
                }
            }

            @Override
            public void onDialogNegativeClick(LogNameDialogFragment dialog) {
                Objects.requireNonNull(dialog.getDialog()).cancel();
            }
        });
        dialog.show(getSupportFragmentManager(), "LogNameDialogFragment");
    }


}
