package com.androidblackboxanalyser;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.androidblackboxanalyser.dialog.AlertDialogFragment;
import com.androidblackboxanalyser.service.AnalysisService;

import java.util.ArrayList;

public class AnalysisProgressActivity extends AppCompatActivity {

    public static final String BLACKBOX_LOG_URI_KEY = "BLACKBOX_LOG_URI_KEY";
    public static final String ANALYSIS_NAME_KEY = "ANALYSIS_NAME_KEY";
    private static final String LOG_TAG = AnalysisProgressActivity.class.getName();

    private ProgressBar progressBar;
    private TextView progressTextView;
    protected Handler handler = new Handler(Looper.getMainLooper());


    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 100 && resultCode==200) {
            String action = intent.getAction();
            switch (action){
                case AnalysisService.ACTION_PROGRESS :
                    int max = intent.getIntExtra(AnalysisService.EXTRA_PROGRESS_MAX,0);
                    int progress = intent.getIntExtra(AnalysisService.EXTRA_PROGRESS,0);
                    String text = intent.getStringExtra(AnalysisService.EXTRA_PROGRESS_TEXT);
                    updateProgress(text,max,progress);
                    break;
                case AnalysisService.ACTION_START_DETAIL_AND_STOP_PROGRESS :
                    ArrayList<Record> recordList = intent.getParcelableArrayListExtra(AnalysisService.EXTRA_RECORD_LIST);
                    startDetailActivityAndStopProgressActivity(recordList);
                    break;
                case AnalysisService.ACTION_ERROR :
                    String msg = intent.getStringExtra(AnalysisService.EXTRA_ERROR_MSG);
                    DialogFragment dialog = new AlertDialogFragment(msg);
                    dialog.show(getSupportFragmentManager(), "ERROR");
                    Log.d(LOG_TAG, "broadcastReceiver updateProgress");
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_analysis_progress);
        Uri fileUri = getIntent().getParcelableExtra(BLACKBOX_LOG_URI_KEY);
        String name = getIntent().getStringExtra(ANALYSIS_NAME_KEY);
        if (fileUri != null && name != null) {
            this.progressBar = findViewById(R.id.progressBar);
            this.progressTextView = findViewById(R.id.progress_text);
            AnalysisService.startActionAnalysis(this,fileUri,name);
        } else {
            finish();
        }

    }

    public void updateProgress(String text, int max, int progress) {
        this.progressBar.setMax(max);
        this.progressBar.setProgress(progress);
        this.progressTextView.setText(text);
    }

    public void startDetailActivityAndStopProgressActivity(ArrayList<Record> recordList) {
        if(recordList !=null && !recordList.isEmpty()) {
            Intent intent = new Intent(this, DetailsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putParcelableArrayListExtra(DetailsActivity.RECORD_LIST_KEY, recordList);
            intent.putExtra(DetailsActivity.
                    RECORD_KEY, recordList.get(0));
            startActivity(intent);
        }
        finish();
    }
}