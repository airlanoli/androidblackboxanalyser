package com.androidblackboxanalyser.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.DialogFragment;

import com.androidblackboxanalyser.R;
import com.androidblackboxanalyser.UsbDeviceAdapter;
import com.androidblackboxanalyser.utils.USBUtil;

import java.util.ArrayList;
import java.util.Map;

public class USBDialogFragment extends DialogFragment {


    private static final String LOG_TAG = USBDialogFragment.class.getName();
    private AppCompatSpinner spinner;
    private UsbManager manager;

    private static final String ACTION_USB_PERMISSION =
            "com.android.example.USB_PERMISSION";
    private final BroadcastReceiver usbReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if(device != null){

                            UsbDeviceConnection connection  = manager.openDevice(device);

                            if (connection!=null) {
                                if (!USBUtil.isMassStorage(connection, device)) {
                                    USBUtil.activateMassStorage(connection, device);
                                    connection.close();
                                    DialogFragment dialog = new AlertDialogFragment(getString(R.string.alert_mass_storage_ok));
                                    dialog.show(((AppCompatActivity)context).getSupportFragmentManager(), "AlertDiologMAssStorageOK");
                                    dismiss();
                                }
                            }else{
                                DialogFragment dialog = new AlertDialogFragment(getString(R.string.alert_usb_connection_lost));
                                dialog.show(((AppCompatActivity)context).getSupportFragmentManager(), "AlertDialogUSBConnectionLost");
                                dismiss();
                            }

                        }
                    }
                    else {
                        Log.d(LOG_TAG, "permission denied for device " + device);
                    }
                }
            }
        }
    };


    public USBDialogFragment() {
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View mView = inflater.inflate(R.layout.dialog_usb, null);


        //register the broadcast receiver
        PendingIntent permissionIntent = PendingIntent.getBroadcast(getContext(), 0, new Intent(ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        getContext().registerReceiver(usbReceiver, filter);

        manager = (UsbManager)getContext().getSystemService(Context.USB_SERVICE);

        Map<String, UsbDevice> devices = manager.getDeviceList();
        ArrayList<UsbDevice> deviceList = new ArrayList<>();

        if (devices != null
                && devices.size() > 0) {
            Log.d(LOG_TAG, "Found " + devices.size() + " devices");

            for (Map.Entry<String, UsbDevice> entry : devices.entrySet()) {
                deviceList.add(entry.getValue());

                Log.d(LOG_TAG, entry.getKey() + "/ getDeviceName : " + entry.getValue().getDeviceName());
                Log.d(LOG_TAG, entry.getKey() + "/ getDeviceId : " + entry.getValue().getDeviceId());
                Log.d(LOG_TAG, entry.getKey() + "/ getProductId : " + entry.getValue().getProductId());
                Log.d(LOG_TAG, entry.getKey() + "/ getDeviceProtocol : " + entry.getValue().getDeviceProtocol());
            }
        }


        if (deviceList != null
                && deviceList.size() > 0) {

            spinner = mView.findViewById(R.id.usb_spinner);
            UsbDeviceAdapter adapter = new UsbDeviceAdapter(getContext(), deviceList);
            spinner.setAdapter(adapter);

        } else {
           Log.e(LOG_TAG,"Empty or null USB device list in create dialog.");
        }

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        AlertDialog dialog =  builder.setView(mView)
                // Add action buttons
                .setPositiveButton(R.string.ok,null)
                .create();
        dialog.setOnShowListener(dialogInterface -> {

            Button button = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(view -> {
                UsbDevice device = (UsbDevice) spinner.getSelectedItem();
                if( device !=null){

                    //Explicitly asking for permission
                    PendingIntent mPermissionIntent = PendingIntent.getBroadcast(getContext(), 0, new Intent(ACTION_USB_PERMISSION), 0);
                    manager.requestPermission(device, mPermissionIntent);

                }
            });

        });
        return dialog;
    }
}
