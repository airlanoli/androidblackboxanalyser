package com.androidblackboxanalyser;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;

import com.androidblackboxanalyser.dialog.AboutDialogFragment;
import com.androidblackboxanalyser.dialog.AlertDialogFragment;
import com.androidblackboxanalyser.dialog.LogNameDialogFragment;
import com.androidblackboxanalyser.dialog.OpenAnalysisDialogFragment;
import com.androidblackboxanalyser.dialog.USBDialogFragment;
import com.androidblackboxanalyser.utils.BlackBoxUtils;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

abstract class AbstractActivityWithMenu extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, LogNameDialogFragment.LogNameDialogListener {




    enum DialogEnum {NONE, LOG_NAME_DIALOG}
    private DialogEnum dialogEnum = DialogEnum.NONE;

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private static final int MY_RESULT_CODE_FILECHOOSER = 2;
    private static final String LOG_TAG = AbstractActivityWithMenu.class.getName();

    private Uri blackBoxLogFile =null;

    //FOR DESIGN
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;

    protected abstract int getActivityLayout();

    protected abstract void configureMainView();

    @Override
    public void onDialogPositiveClick(LogNameDialogFragment dialog) {

        String name = dialog.getLogNameInput();

        if (name != null && !name.isEmpty()) {
            if(name.contains("/")){
                dialog.setError(getString(R.string.error_name_with_slash));
            }else if(BlackBoxUtils.isRecordAlreadyExist(name,this)){
                dialog.setError(getString(R.string.error_record_exist,name));
            }else{
                dialog.dismiss();
                Intent intent = new Intent(this, AnalysisProgressActivity.class);
                intent.putExtra(AnalysisProgressActivity.BLACKBOX_LOG_URI_KEY,this.blackBoxLogFile);
                intent.putExtra(AnalysisProgressActivity.ANALYSIS_NAME_KEY,name);
                startActivity(intent);
            }
        }else{
            dialog.setError(getString(R.string.error_record_name_empty));
        }
    }

    @Override
    public void onDialogNegativeClick(LogNameDialogFragment dialog) {
        Objects.requireNonNull(dialog.getDialog(),"Dialog must ot be null").cancel();
    }


    private void askPermissionAndBrowseFile() {
        // Check if we have Call permission
        int permission = ActivityCompat.checkSelfPermission(AbstractActivityWithMenu.this, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // If don't have permission so prompt the user.
            ActivityCompat.requestPermissions(AbstractActivityWithMenu.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            return;
        }
        this.doBrowseFile();
    }

    private void doBrowseFile() {
        Intent chooseFileIntent = new Intent(Intent.ACTION_GET_CONTENT);
        chooseFileIntent.setType("*/*");
        // Only return URIs that can be opened with ContentResolver
        chooseFileIntent.addCategory(Intent.CATEGORY_OPENABLE);

        chooseFileIntent = Intent.createChooser(chooseFileIntent, "Choose a file");
        startActivityForResult(chooseFileIntent, MY_RESULT_CODE_FILECHOOSER);
    }

    // When you have the request results
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {// Note: If request is cancelled, the result arrays are empty.
            // Permissions granted (CALL_PHONE).
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.i(LOG_TAG, "Permission granted!");
                Toast.makeText(AbstractActivityWithMenu.this, "Permission granted!", Toast.LENGTH_SHORT).show();

                this.doBrowseFile();
            }
            // Cancelled or denied.
            else {
                Log.i(LOG_TAG, "Permission denied!");
                Toast.makeText(AbstractActivityWithMenu.this, "Permission denied!", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MY_RESULT_CODE_FILECHOOSER) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    this.blackBoxLogFile = data.getData();
                    Log.i(LOG_TAG, "Uri: " + this.blackBoxLogFile);
                    //set enum to show logNameFragment after close this one , ( see onResumeFragments() )
                    dialogEnum = DialogEnum.LOG_NAME_DIALOG;
                }
            } else {
                this.blackBoxLogFile = null;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();

        // play with fragments here
        if (dialogEnum == DialogEnum.LOG_NAME_DIALOG) {// Create an instance of the dialog fragment and show it
            LogNameDialogFragment dialog = new LogNameDialogFragment(getString(R.string.dialog_name_label));
            dialog.setListener(this);
            dialog.show(getSupportFragmentManager(), "LogNameDialogFragment");
            dialogEnum = DialogEnum.NONE;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getActivityLayout());

        // Configure all views
        this.configureToolBar();
        this.configureDrawerLayout();
        this.configureNavigationView();
        //call abstract method
        this.configureMainView();
    }


    @Override
    public void onBackPressed() {
        // 5 - Handle back click to close menu
        if (this.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Handle Navigation Item Click
        int id = item.getItemId();

        switch (id) {
            case R.id.activity_main_menu_drawer_new:
                askPermissionAndBrowseFile();
                break;
            case R.id.activity_main_menu_drawer_open:
                openExistingAnalysis();
                break;
            case R.id.activity_main_menu_drawer_manage:
                manageAnalysis();
                break;
            case R.id.activity_main_menu_drawer_about:
                openAboutDialog();
                break;
            case R.id.activity_main_menu_drawer_usb:
                openUSBDialog();
                break;
            default:
                break;
        }

        this.drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }

    private void openUSBDialog() {
        PackageManager myPackageManager = getPackageManager();
        Boolean USBHostSupported = myPackageManager.hasSystemFeature(PackageManager.FEATURE_USB_HOST);
        if (!USBHostSupported) {
            Log.d(LOG_TAG, "Device does't support Usb");

            DialogFragment dialog = new AlertDialogFragment(getString(R.string.alert_no_usb_support));
            dialog.show(getSupportFragmentManager(), "AlertDiologUSBHostNotSupported");

        }else {

            UsbManager mUsbManager = (UsbManager)getSystemService(Context.USB_SERVICE);

            Map<String, UsbDevice> devices = mUsbManager.getDeviceList();
            ArrayList<UsbDevice> deviceList = new ArrayList<>();

            if (devices != null
                    && devices.size() > 0) {

                DialogFragment dialog = new USBDialogFragment();
                dialog.show(getSupportFragmentManager(), "USBDialogFragment");

            } else {
                DialogFragment dialog = new AlertDialogFragment(getString(R.string.alert_no_usb_device));
                dialog.show(getSupportFragmentManager(), "AlertDiologUSBHostNotSupported");
            }



        }
    }


    private void manageAnalysis() {
        Intent intent = new Intent(this, ManageAnalysisActivity.class);
        startActivity(intent);
    }

    private void openAboutDialog(){
        DialogFragment dialog = new AboutDialogFragment();
        dialog.show(getSupportFragmentManager(), "AboutDialogFragment");
    }

    private void openExistingAnalysis() {
        String[] tab = BlackBoxUtils.listRecord(this);
        if( tab != null && tab.length >0 ) {
            DialogFragment dialog = new OpenAnalysisDialogFragment(tab);
            dialog.show(getSupportFragmentManager(), "OpenAnalysisDialogFragment");
        }else{
            DialogFragment dialog = new AlertDialogFragment(getString(R.string.alert_no_analysis));
            dialog.show(getSupportFragmentManager(), "AlertDiologFragmentNoAnalysis");
        }
    }

    // ---------------------
    // CONFIGURATION
    // ---------------------

    //  Configure Toolbar
    private void configureToolBar() {
        this.toolbar = findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(toolbar);
    }

    //  Configure Drawer Layout
    private void configureDrawerLayout() {
        this.drawerLayout = findViewById(R.id.activity_main_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    //  Configure NavigationView
    private void configureNavigationView() {
        NavigationView navigationView = findViewById(R.id.activity_main_nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }



}
