package com.androidblackboxanalyser.utils;

import android.content.Context;
import android.util.JsonReader;
import android.util.JsonWriter;

import com.androidblackboxanalyser.Record;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class JSonUtils implements UtilsContants {

    public static void writeJson(String recordName, List<Record> records, Context context) throws IOException {
        File f = new File(context.getApplicationContext().getFilesDir(), RECORDS_DIR_NAME);
        f = new File(f, recordName);
        f = new File(f, RECORDS_JSON_FILE);
        OutputStream out = new FileOutputStream(f);
        writeJsonStream(out, records);
        out.close();
    }

    public static void writeJsonStream(OutputStream out, List<Record> records) throws IOException {
        JsonWriter writer = new JsonWriter(new OutputStreamWriter(out, StandardCharsets.UTF_8));
        writer.setIndent("  ");
        writeRecordsArray(writer, records);
        writer.close();
    }

    private static void writeRecordsArray(JsonWriter writer, List<Record> records) throws IOException {
        writer.beginArray();
        for (Record record : records) {
            writeRecord(writer, record);
        }
        writer.endArray();
    }

    private static void writeRecord(JsonWriter writer, Record record) throws IOException {
        writer.beginObject();
        writer.name("number").value(record.getNumber());
        writer.name("headerMap");
        writeHeader(writer, record.getHeaderMap());
        writer.endObject();
    }

    private static void writeHeader(JsonWriter writer, TreeMap<String, String> headerMap) throws IOException {

        writer.beginObject();
        for (Map.Entry<String, String> entry : headerMap.entrySet()) {
            writer.name(entry.getKey()).value(entry.getValue());
        }
        writer.endObject();
    }

    public static ArrayList<Record> readJson(String recordName, Context context) throws IOException {
        File f = new File(context.getApplicationContext().getFilesDir(), RECORDS_DIR_NAME);
        f = new File(f, recordName);
        f = new File(f, RECORDS_JSON_FILE);
        InputStream in = new FileInputStream(f);
        return readJsonStream(recordName, in);
    }

    public static ArrayList<Record> readJsonStream(String recordName, InputStream in) throws IOException {
        try (JsonReader reader = new JsonReader(new InputStreamReader(in, StandardCharsets.UTF_8))) {
            return readRecordsArray(recordName, reader);
        }
    }

    private static ArrayList<Record> readRecordsArray(String recordName, JsonReader reader) throws IOException {
        ArrayList<Record> records = new ArrayList<>();
        reader.beginArray();
        while (reader.hasNext()) {
            records.add(readRecord(recordName, reader));
        }
        reader.endArray();
        return records;
    }

    private static Record readRecord(String name, JsonReader reader ) throws IOException {


        int number = -1;
        TreeMap<String, String> headerMap = null;

        reader.beginObject();
        while (reader.hasNext()) {

            String currentName = reader.nextName();
            switch (currentName) {
                case "number":
                    number = reader.nextInt();
                    break;
                case "headerMap":
                    headerMap = readHeader(reader);
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();

        return new Record(name, number, headerMap);
    }

    private static TreeMap<String, String> readHeader(JsonReader reader) throws IOException {
        TreeMap<String, String> headerMap = new TreeMap<>();
        reader.beginObject();
        while (reader.hasNext()) {
            headerMap.put(reader.nextName(), reader.nextString());
        }
        reader.endObject();
        return headerMap;
    }


}
