package com.androidblackboxanalyser.utils;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.androidblackboxanalyser.Record;
import com.chaquo.python.PyObject;
import com.chaquo.python.Python;
import com.chaquo.python.android.AndroidPlatform;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.TreeMap;

public class BlackBoxUtils implements UtilsContants {

    private static final String LOG_TAG = BlackBoxUtils.class.getName();


    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }


    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public static native void parseLogFromJNI(String path);

    private static boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        return directoryToBeDeleted.delete();
    }

    private static void copyfile(Uri inputUri, File dest, boolean replaceExisting, Context context) {
        if (replaceExisting && dest.exists()) {
            dest.delete();
        }

        try (
                InputStream in = context.getContentResolver().openInputStream(inputUri);
                OutputStream out = new BufferedOutputStream(
                        new FileOutputStream(dest))) {

            byte[] buffer = new byte[1024];
            int lengthRead;
            assert in != null;
            while ((lengthRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, lengthRead);
                out.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static File copyOnTempDir(Uri inputUri, Context context) throws IOException {
        File tmpDir = new File(context.getApplicationContext().getFilesDir(), TMP_DIR_NAME);
        //clean temp dir

        if (tmpDir.exists()) {
            deleteDirectory(tmpDir);
        }
        File destFile = null;
        if (tmpDir.mkdir()) {
            destFile = new File(tmpDir, BLACKBOX_MAIN_LOG_FILE);
            copyfile(inputUri, destFile, true, context);
        }
        return destFile;

    }


    public static void callPlasmatree(File file, Context context, int logNumber) {
        File tmpDir = new File(context.getApplicationContext().getFilesDir(), TMP_DIR_NAME);

        List<String> filePathList = new ArrayList<>();

        // the normal way to use plasmatree is a list of file but we launch it one per one to display progress to the user
        filePathList.add(file.getName());

        Log.i(LOG_TAG, "liste log :  " + filePathList.toString());
        Log.i(LOG_TAG, "tmpdir :  " + tmpDir.getAbsolutePath());

        if (!Python.isStarted()) {
            Python.start(new AndroidPlatform(context));
            Log.i(LOG_TAG,"Start Python");
        }
        Python py = Python.getInstance();
        PyObject pym = py.getModule("my_pid_analyser");
        pym.callAttr("run_analysis", filePathList.toArray(), tmpDir.getAbsolutePath(), IMAGE_NAME + logNumber);
        pym.close();

    }

    public static Uri getRecordResponseImageURI(Record record, Context context) {
        return getRecordFileURI(record, PLASMATREE_RESPONSE_GRAPH_FILE_PATTERN, context);
    }

    public static Uri getRecordNoiseImageURI(Record record, Context context) {
        return getRecordFileURI(record, PLASMATREE_NOISE_GRAPH_FILE_PATTERN, context);
    }

    public static Uri getRecordFileURI(Record record, String filePattern, Context context) {
        Uri uri = null;
        if (record != null && context != null) {
            uri = Uri.withAppendedPath(Uri.fromFile(context.getApplicationContext().getFilesDir()), RECORDS_DIR_NAME);
            uri = Uri.withAppendedPath(uri, record.getName());
            uri = Uri.withAppendedPath(uri, String.format(filePattern, record.getNumber()));
        }
        return uri;
    }

    public static boolean moveTmpDirToRecord(String recordName, Context context) throws IOException {
        File recordsDir = new File(context.getApplicationContext().getFilesDir(), RECORDS_DIR_NAME);
        if (!recordsDir.exists()) {
            if (!recordsDir.mkdir()) {
                return false;
            }
        }

        File dest = new File(recordsDir, recordName);
        if (dest.exists()) {
            return false;
        }

        File src = new File(context.getApplicationContext().getFilesDir(), TMP_DIR_NAME);
        src.renameTo(dest);
        return true;
    }

    public static boolean moveRecord(String oldName, String newName, Context context) throws IOException {
        File recordsDir = new File(context.getApplicationContext().getFilesDir(), RECORDS_DIR_NAME);
        File src = new File(recordsDir, oldName);
        File dest = new File(recordsDir, newName);
        if (dest.exists()) {
            return false;
        }
        src.renameTo(dest);
        return true;
    }


    public static List<File> splitBBLog(File file, Context context) throws IOException {
        ArrayList<File> fileList = new ArrayList<>();
        byte[] allByte = new byte[(int) file.length()];

        // can't use Files.readAllBytes(path) for sdk23 compatibility
        try(FileInputStream fis = new FileInputStream(file)){
            fis.read(allByte);
        }

        int indexOfFirstEndLine = -1;
        for (int i = 0; i < allByte.length; i++) {
            if (allByte[i] == '\n') {
                indexOfFirstEndLine = i;
                break;
            }
        }
        if (indexOfFirstEndLine < 0) {
            Log.e(LOG_TAG, "no new line in file : " + file.getAbsolutePath());
            return fileList;
        }
        byte[] firstLine = Arrays.copyOfRange(allByte, 0, indexOfFirstEndLine + 1);

        //inti index and log counter
        int lastFirstIndex = 0;

        for (int i = firstLine.length; i < allByte.length; i++) {
            if (allByte[i] == firstLine[0]) {
                boolean found = true;
                for (int j = 1; found && j < firstLine.length; j++) {
                    found = allByte[i + j] == firstLine[j];
                }
                if (found) {
                    //There is a new log start on i index , write file with sub array before i
                    byte[] currentByte = Arrays.copyOfRange(allByte, lastFirstIndex, i);
                    File destFile = splitedLogFileAtIndex(context,fileList.size());
                    try (FileOutputStream fos = new FileOutputStream(destFile)) {
                        fos.write(currentByte);
                    }
                    if (destFile.length() > LOG_MIN_BYTES) {
                        fileList.add(destFile);
                    } else {
                        Log.w(LOG_TAG, "logFile " + destFile.getAbsolutePath() + " is to small (" + destFile.length() + " bytes ) need is " + LOG_MIN_BYTES + "bytes");
                        if (!destFile.delete()) {
                            Log.e(LOG_TAG, "Can't delete logFile " + destFile.getAbsolutePath());
                        }
                    }
                    //init counter and index to the next log start
                    lastFirstIndex = i;
                    i += firstLine.length;

                }
            }

        }
        //write the last log
        byte[] currentByte = Arrays.copyOfRange(allByte, lastFirstIndex, allByte.length);
        File destFile = splitedLogFileAtIndex(context,fileList.size());
        try (FileOutputStream fos = new FileOutputStream(destFile)) {
            fos.write(currentByte);
        }
        fileList.add(destFile);

        return fileList;

    }

    public static String splitedLogfileNameAtIndex(int i){
        return SPLITED_BLACKBOX_LOG_NAME + String.format(Locale.FRENCH, "%02d", i) + BLACKBOX_LOG_EXTENTION;
    }

    public static File splitedLogFileAtIndex(Context context, int i){
        File tmpDir = new File(context.getApplicationContext().getFilesDir(), TMP_DIR_NAME);
        return new File(tmpDir, splitedLogfileNameAtIndex(i));
    }

    public static boolean isRecordAlreadyExist(String recordName, Context context) {
        File recordsListDir = new File(context.getApplicationContext().getFilesDir(), RECORDS_DIR_NAME);
        File recordDir = new File(recordsListDir, recordName);
        return recordDir.exists();
    }

    public static String[] listRecord(Context context) {
        File recordsListDir = new File(context.getApplicationContext().getFilesDir(),
                RECORDS_DIR_NAME);
        ArrayList<String> list = new ArrayList<>();

        File [] fileArray = recordsListDir.listFiles();
        if(fileArray !=null) {
            for (File f : fileArray) {

                if (f.isDirectory()) {
                    list.add(f.getName());
                }
            }
            return list.toArray(new String[0]);
        }else{
            return new String[0];
        }

    }

    public static TreeMap<String, String> readHeaderFromFile(File f) {

        TreeMap<String, String> map = new TreeMap<>();

        byte[] allByte = new byte[(int) f.length()];

        // can't use Files.readAllBytes(path) for sdk23 compatibility
        try(FileInputStream fis = new FileInputStream(f)){
            fis.read(allByte);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < allByte.length - 1; i++) {
            if (allByte[i] == 'H' && allByte[i + 1] == ' ') { // header
                for (int j = i + 2; j < allByte.length; j++) {
                    if (allByte[j] == '\n') {
                        //it's the end of this line
                        String str = new String(Arrays.copyOfRange(allByte, i + 2, j), StandardCharsets.UTF_8);
                        String[] tab = str.split(":");
                        if (tab.length == 2) {
                            map.put(tab[0], tab[1]);
                        }
                        i = j;
                        break;
                    }
                }
            } else {
                //it's the end of header.
                break;
            }
        }
        return map;
    }

    public static void cleanRecordDir(String recordName, Context context) {
        File recDir = new File(context.getApplicationContext().getFilesDir(), RECORDS_DIR_NAME);
        final File dir = new File(recDir, recordName);

        if (dir.exists()) {
            File[] allContents = dir.listFiles();
            if(allContents!=null){
                for (File f : allContents) {
                    if(!f.getName().matches(REGEXP_FILE_TO_KEEP) ){
                        f.delete();
                    }
                }

            }
        }
    }

    public static void deleteLogs(String recordName, Context context) throws IOException {
        File recDir = new File(context.getApplicationContext().getFilesDir(), RECORDS_DIR_NAME);
        final File dir = new File(recDir, recordName);

        deleteDirectory(dir);
    }
}
