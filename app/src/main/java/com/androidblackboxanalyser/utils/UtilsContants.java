package com.androidblackboxanalyser.utils;

public interface UtilsContants {

    String TMP_DIR_NAME = "tmp";
    String RECORDS_DIR_NAME = "records";
    String SPLITED_BLACKBOX_LOG_NAME = "blackbox_log-";
    String IMAGE_NAME = "image_";
    String FULL_IMAGE_PREFIX = "blackbox_" + IMAGE_NAME;
    String BLACKBOX_LOG_EXTENTION = ".bbl";
    String BLACKBOX_MAIN_LOG_FILE = "blackbox_log.bbl";
    String PLASMATREE_NOISE_GRAPH_FILE_PATTERN = FULL_IMAGE_PREFIX + "%d_0_noise.png";
    String PLASMATREE_RESPONSE_GRAPH_FILE_PATTERN = FULL_IMAGE_PREFIX + "%d_0_response.png";
    String RECORDS_JSON_FILE = "records.json";
    String REGEXP_FILE_TO_KEEP = "("+FULL_IMAGE_PREFIX+"[0-9]+_0_noise[.]png)|("+FULL_IMAGE_PREFIX+"[0-9]+_0_response[.]png)|(blackbox_log[.]bbl)|(records[.]json)";
    int LOG_MIN_BYTES = 500000;
}
