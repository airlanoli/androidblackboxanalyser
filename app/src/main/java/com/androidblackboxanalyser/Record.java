package com.androidblackboxanalyser;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

public class Record implements Comparable<Record>, Parcelable {

    private String name;
    private int number;
    private TreeMap<String,String> headerMap;

    public Record(){
        this(null,0);
    }

    public Record(String name, int number) {
        this(name, number, new TreeMap<>());
    }

    public Record(String name, int number,TreeMap<String,String> headerMap) {
        this.name= name;
        this.number = number;
        this.headerMap = headerMap;
    }

    protected Record(Parcel in) {
        this.name = in.readString();
        this.number = in.readInt();
        this.headerMap = new TreeMap<>();
        int cpt = in.readInt();
        for(int i=0;i<cpt;i++ ){
            String key = in.readString();
            String value = in.readString();
            this.headerMap.put(key,value);
        }
    }

    public static final Creator<Record> CREATOR = new Creator<Record>() {
        @Override
        public Record createFromParcel(Parcel in) {
            return new Record(in);
        }

        @Override
        public Record[] newArray(int size) {
            return new Record[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeInt(number);
        parcel.writeInt(this.headerMap.size());
        for(Map.Entry<String, String> entry : this.headerMap.entrySet()){
            parcel.writeString(entry.getKey());
            parcel.writeString(entry.getValue());
        }
    }

    @Override
    public int compareTo(Record record) {
        return this.number - record.getNumber();
    }

    @NonNull
    @Override
    public String toString() {
        return name + " : " + number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Record record = (Record) o;
        return number == record.number &&
                Objects.equals(name, record.name) && Objects.equals(headerMap,record.getHeaderMap());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, number);
    }

    public TreeMap<String, String> getHeaderMap() {
        return headerMap;
    }

    public void setHeaderMap(TreeMap<String, String> headerMap) {
        this.headerMap = headerMap;
    }

    public void putHeader(String key,String value){
        this.headerMap.put(key,value);
    }

    public String getHeader(String key){
        return this.headerMap.get(key);
    }

}
