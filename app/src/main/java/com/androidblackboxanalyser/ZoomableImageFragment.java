package com.androidblackboxanalyser;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.github.chrisbanes.photoview.PhotoView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ZoomableImageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ZoomableImageFragment extends Fragment {




    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    protected static final String IMAGE_URI_KEY = "IMAGE_URI_KEY";

    protected Uri imageUri;


    public ZoomableImageFragment() {
        // Required empty public constructor
    }






    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.imageUri = getArguments().getParcelable(IMAGE_URI_KEY);
        }
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param imageUri the uri of image to display
     * @return A new instance of fragment HeaderFragment.
     */
    public static ZoomableImageFragment newInstance(Uri imageUri) {
        ZoomableImageFragment fragment = new ZoomableImageFragment();
        Bundle args = new Bundle();
        args.putParcelable(IMAGE_URI_KEY, imageUri);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_zoomable_image, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PhotoView photoView = (PhotoView) getView().findViewById(R.id.photo_view);
        photoView.setImageURI(this.imageUri);
        photoView.setMinimumScale(1);
        photoView.setMaximumScale(5);
    }
}