package com.androidblackboxanalyser;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HeaderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HeaderFragment extends Fragment {


    public static final String D_MIN = "d_min";
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    protected static final String RECORD_KEY = "RECORD_KEY";
    private static final String PITCH_PID = "pitchPID";
    private static final String ROLL_PID = "rollPID";
    private static final String YAW_PID = "yawPID";
    public static final String FEEDFORWARD_WEIGHT = "feedforward_weight";

    protected Record record;

    public HeaderFragment() {
        // Required empty public constructor
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.record = getArguments().getParcelable(RECORD_KEY);
        }
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param record the record
     * @return A new instance of fragment HeaderFragment.
     */
    public static HeaderFragment newInstance(Record record) {
        HeaderFragment fragment = new HeaderFragment();
        Bundle args = new Bundle();
        args.putParcelable(RECORD_KEY, record);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_header, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView textView = null;

        fillPID(PITCH_PID,R.id.header_tab_pitch_P,R.id.header_tab_pitch_I,R.id.header_tab_pitch_D);
        fillPID(ROLL_PID,R.id.header_tab_roll_P,R.id.header_tab_roll_I,R.id.header_tab_roll_D);
        fillPID(YAW_PID,R.id.header_tab_yaw_P,R.id.header_tab_yaw_I,R.id.header_tab_yaw_D);
        fillPID(D_MIN,R.id.header_tab_roll_D_min,R.id.header_tab_pitch_D_min,R.id.header_tab_yaw_D_min);
        fillPID(FEEDFORWARD_WEIGHT,R.id.header_tab_roll_FF,R.id.header_tab_pitch_FF,R.id.header_tab_yaw_FF);

        //textView.setText(record.getName() + " Record : "+record.getNumber());

        TypedValue out = new TypedValue();
        getResources().getValue(R.dimen.header_tab_key_layout_weight, out, true);
        float keyLayoutWeight = out.getFloat();
        getResources().getValue(R.dimen.header_tab_value_layout_weight, out, true);
        float valueLayoutWeight = out.getFloat();

        TableLayout tl = (TableLayout) getView().findViewById(R.id.header_raw_tab);

        for(Map.Entry<String,String> entry : record.getHeaderMap().entrySet()) {
            // Create a new row to be added.
            TableRow tr = new TableRow(getContext());
            tr.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT));

            // Create a textView to be the row-content.
            textView = new TextView(getContext());
            textView.setText(entry.getKey());
            textView.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT,keyLayoutWeight));
            textView.setBackgroundResource(R.drawable.border);
            tr.addView(textView);
            textView = new TextView(getContext());
            textView.setText(entry.getValue());
            textView.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT,valueLayoutWeight));
            textView.setBackgroundResource(R.drawable.border);
            tr.addView(textView);

            // Add row to TableLayout.
            tl.addView(tr);
        }


    }

    private void fillPID(String header, int id0, int id1, int id2) {
        String pid = record.getHeader(header);
        if(pid!=null){
            String[] tab = pid.split(",");
            if(tab.length==3){
                TextView textView = (TextView) getView().findViewById(id0);
                textView.setText(tab[0]);
                textView = (TextView) getView().findViewById(id1);
                textView.setText(tab[1]);
                textView = (TextView) getView().findViewById(id2);
                textView.setText(tab[2]);
            }
        }
    }
}