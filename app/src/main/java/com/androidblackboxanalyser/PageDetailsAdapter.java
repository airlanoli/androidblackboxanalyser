package com.androidblackboxanalyser;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.androidblackboxanalyser.utils.BlackBoxUtils;

public class PageDetailsAdapter extends FragmentStateAdapter {

    private Record record;
    private Context context;

    // 2 - Default Constructor
    public PageDetailsAdapter(FragmentActivity fragmentActivity, Record record) {
        super(fragmentActivity);
        this.record = record;
        this.context = fragmentActivity;

    }

    @Override
    public int getItemCount() {
        return 3;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0 :
                return HeaderFragment.newInstance(record);
            case 1 :
                return ZoomableImageFragment.newInstance(BlackBoxUtils.getRecordNoiseImageURI(record,this.context));
            case 2 :
                return ZoomableImageFragment.newInstance(BlackBoxUtils.getRecordResponseImageURI(record,this.context));
        }
        return null;
    }
}
